using System;
using System.Linq;
using System.Collections.Generic;
using static System.Console;

public static class main
{
	public static int Main(string[] args)
	{
		if (args.Length == 0)
		{throw new System.ArgumentException($"\n no argument given. Please give file containning IntCode");}

		WriteLine();
		WriteLine(" DAY 2 : IntCode");
		/* ----------------------------------------------------------- Part 1 */
		WriteLine();
		WriteLine("--- Part 1 ---");
		/* read IntCode program */
		int[] inCode = DataReader.ReadOneLine2Int(args[0], args[1]);

		/* making computer working again */
		inCode[1] = 12;
		inCode[2] = 2;
		
		/* copy IntCode */
		int[] outCode = new int[inCode.Length];
		System.Array.Copy(inCode, outCode, inCode.Length);

		/* in place computation */
		int ops = AoC19.IntCodeReader(outCode, printout:false);

		/* printing part 1 result */

		WriteLine($"Operations before Halt = {ops}");
		WriteLine($"outCode position {0} = {outCode[0]}");

		/* ----------------------------------------------------------- Part 2 */
		WriteLine("\n--- Part 2 ---");
		/* List containing noun and verb combinations */
		List<int[]> nounverb = new List<int[]>();
		int n = 12;
		int v = 2;
		nounverb.Add(new int[] {n, v});

		int tries = 0;
		Random ranInt = new Random();
		while (outCode[0]!=19690720)
		{
			while (nounverb.Any(p => p.SequenceEqual(new int[] {n, v})))
			{
				n = ranInt.Next(0,100);
				v = ranInt.Next(0,100);
			}
			nounverb.Add(new int[] {n, v});
			/* reset outCode */
			System.Array.Copy(inCode, outCode, inCode.Length);
			/* input new values */
			outCode[1] = n;
			outCode[2] = v;
			/* run intCode program */
			AoC19.IntCodeReader(outCode, printout:false);
			tries++;
		}
		WriteLine($"Tries before success = {tries}");
		WriteLine($"100*noun + verb = 100*{n} + {v}  = {100*n+v} \n");
		return 0;
	}
}
