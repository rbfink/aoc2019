using System;

public static partial class AoC19
{
    public static int[] Int2IntArray(int input)
    {
        int num = Math.Abs(input);
        // find length of input
        int inputLength = (int)Math.Floor(Math.Log10(num)) + 1;
        // initiate array
        int[] returnArray = new int[inputLength];
        // fill out array
        for (int i=inputLength-1; i>=0; i--)
        {
            int mod = num % 10; // seperate last digit
            returnArray[i] = mod;
            num = num / 10;
        }
        return returnArray;
    }
}