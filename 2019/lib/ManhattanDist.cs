using static System.Math;

public static partial class AoC19
{
	public static int ManhattanDist(int[] coord1, int[] coord2)
	{
		return Abs(coord1[0] - coord2[0]) + Abs(coord1[1] - coord2[1]);
	}//ManhattanDist
}//AoC19
