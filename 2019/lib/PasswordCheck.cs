using System;
using System.Linq;
using static System.Console;

public static partial class AoC19
{
    public static bool PasswordCheck(int passwordCandidate, bool extraCheck)
    {
        bool passwordUseable = new bool();
        bool extraCheckPass = false;

        // find length of password input
        int passwordLength = (int)Math.Floor(Math.Log10(Math.Abs(passwordCandidate))) + 1;
        // create int[] array with password digits
        int[] passwordArray = AoC19.Int2IntArray(passwordCandidate);

        // Check password conditions
        for (int i=0; i<passwordLength-1; i++)
        {
            // condition 1 : never decending digits
            if (passwordArray[i] > passwordArray[i+1])
            {
                passwordUseable = false;
                return passwordUseable;
            }
            // condition 2 with part 2 check
            if (extraCheck && passwordArray[i] == passwordArray[i+1])
            {
                // check if password already passed
                if (extraCheckPass)
                {
                    continue;
                }
                // check for new condition
                else if (2==passwordArray.Count(x => x == passwordArray[i]))
                {
                    extraCheckPass = true;
                    passwordUseable = true;
                }
                else 
                {
                    passwordUseable = false;
                }
            }

            // condition 2 : minimum two neighbors must be duplicates
            else if (passwordArray[i] == passwordArray[i+1])
            {
                passwordUseable = true;
            }
        }
    return passwordUseable;
    }
}