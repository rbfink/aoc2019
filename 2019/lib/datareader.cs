using System.Collections.Generic;
using static System.Console;

public class DataReader
{
	public static int[] ReadOneLine2Int(string filename, string sep)
	{
		/* read */
		System.IO.StreamReader reader = System.IO.File.OpenText(filename);
		string line = reader.ReadLine();
		string[] splitline = line.Split(sep);
		int n = splitline.Length;
		/* write */
		int[] returnInt = new int[n];
		for (int i=0; i<n; i++)
		{
			returnInt[i] = int.Parse(splitline[i]);
		}
		return returnInt;
	}

	public static List<int[]> ReadFile2ListInt(string filename)
	{
		/* Method stolen from Marco Majland*/
		string[] lines = System.IO.File.ReadAllLines(filename);
		int n = (lines[0].Split(' ')).Length;
		List<int[]> data = new List<int[]>(n);
		string[] subline;
		for(int i=0;i<n;i++)
		{
			data.Add(new int[lines.Length]);
		}
		for(int i=0;i<lines.Length;i++)
		{
			subline = lines[i].Split(' ');
			for(int j=0;j<n;j++){data[j][i] = int.Parse(subline[j]);}
		}
		return data;
	}//ReadFile2ListInt

	public static List<double[]> ReadFile2ListDouble(string filename)
	{
		/* Method stolen from Marco Majland*/
		string[] lines = System.IO.File.ReadAllLines(filename);
		int n = (lines[0].Split(' ')).Length;
		List<double[]> data = new List<double[]>(n);
		string[] subline;
		for(int i=0;i<n;i++)
		{
			data.Add(new double[lines.Length]);
		}
		for(int i=0;i<lines.Length;i++)
		{
			subline = lines[i].Split(' ');
			for(int j=0;j<n;j++){data[j][i] = double.Parse(subline[j]);}
		}
		return data;
	}//ReadFile2ListDouble
	
	public static List<string[]> ReadFile2ListString(string filename, string sep)
	{
		string[] lines = System.IO.File.ReadAllLines(filename);
		int N = lines.Length;
		List<string[]> data = new List<string[]>(N);
		for (int i=0; i<N; i++)
		{
			/* add string[] with desired capacity to the List element */
			data.Add(new string[lines[i].Length]);
			/* write data to string[] in the List element */
			data[i] = (lines[i].Split(sep));
		}
		return data;
	}//ReadFile2ListString

	public static string[] ReadFile2StringArray(string filename)
	{
		string[] lines = System.IO.File.ReadAllLines(filename);
		return lines;
	}

}//DataReader
