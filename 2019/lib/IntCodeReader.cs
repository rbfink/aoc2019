using System;

public static partial class AoC19
{
	public static int IntCodeReader(int[] input, bool printout=true)
	{
		/* error handling */
		if (input.Length < 4)
		{
			throw new System.ArgumentException("\n IntCode input must be int[] with at least 4 entrances");
		}
		/* reading and executing IntCode in place */
		int operationsBeforeHalt = 0;
		int i = 0;
		while (i<input.Length)
		{
			if (input[i] == 99) // halt intcode program
			{
				if (printout)
				{
					Console.WriteLine();
					Console.WriteLine("--> Halting IntCode program <--");
					Console.WriteLine();
					Console.WriteLine("Operations before halt {0}", operationsBeforeHalt);
					Console.WriteLine();
					break;
				}
				else
				{
					break;
				}
			}
			else if (input[i] == 1) // addition and assignment
			{
				i = Addition(input, i);
			}
			else if (input[i] == 2) // multiplication and assignment
			{
				i = Multiplication(input, i);
			}
			else if (input[i] == 3) // user input and assignment
			{
				i = UserInput(input, i);
			}
			else if (input[i] == 4) // prints value X stored in position [4,X]
			{
				i = PrintOut(input, i);
			}
			else if (input[i] == 5) // jump if true
			{
				i = JumpIfTrue(input, i);
			}
			else if (input[i] == 6) // jump if false
			{
				i = JumpIfFalse(input, i);
			}
			else if (input[i] == 7)	// less than
			{
				i = LessThan(input, i);
			}
			else if (input[i] == 8) // equals
			{
				i = EqualsInt(input, i);
			}
			else if (AoC19.IntLength(input[i]) > 2) // parameter modes
			{
				i = ParameterMode(input, i);
			}
			else // No readable input
			{
				Console.WriteLine("{0}",input[i]);
				throw new System.ArgumentException($"\n Encountered wrong formatting in position {i}");
			}
			operationsBeforeHalt++;
		} // while loop
		return operationsBeforeHalt;
	} // IntCodeReader
	private static int Addition(int[] intcode, int position, int[] opcode=null)
	{
		//Console.WriteLine("Addition");
		if (opcode == null)
		{
			intcode[ intcode[position+3] ] = intcode[ intcode[position+1] ] + intcode[ intcode[position+2] ];
			return position += 4;
		}	
		else
		{
			int A = ParameterReader(intcode, position+1, opcode[opcode.Length-3]);
			int B = ParameterReader(intcode, position+2, opcode[opcode.Length-4]);
			intcode[ intcode[position+3] ] = A + B;
			return position += 4;
		}
	}
	private static int Multiplication(int[] intcode, int position, int[] opcode=null)
	{
		//Console.WriteLine("Multiplication");
		if (opcode == null) // no opcode given (standard position mode)
		{
			intcode[ intcode[position+3] ] = intcode[ intcode[position+1] ] * intcode[ intcode[position+2] ];
			return position += 4;
		}
		else // opcode given : parameter mode
		{
			int A = ParameterReader(intcode, position+1, opcode[opcode.Length-3]);
			int B = ParameterReader(intcode, position+2, opcode[opcode.Length-4]);
			intcode[ intcode[position+3] ] = A * B;
			return position += 4;
		}
	}
	private static int UserInput(int[] intcode, int position, int[] opcode=null)
	{
		//Console.WriteLine("User input");
		if (opcode == null)
		{
			Console.Write("Provide integer IntCode input : ");
			int userInput = Convert.ToInt32( Console.ReadLine() );
			intcode[ intcode[position+1] ] = userInput;
			return position += 2;
		}
		else
		{
			Console.Write("Provide integer IntCode input : ");
			int userInput = Convert.ToInt32( Console.ReadLine() );
			intcode[ intcode[position+1] ] = userInput;
			return position += 2;
		}
	}
	private static int PrintOut(int[] intcode, int position, int[] opcode=null)
	{
		//Console.WriteLine("printout");
		if (opcode == null)
		{
			Console.WriteLine("Intcode printout -> {0}", intcode[ intcode[position+1] ]);
			return position += 2;
		}
		else
		{
			int A = ParameterReader(intcode, position+1, opcode[opcode.Length-3]);
			Console.WriteLine("Intcode printout -> {0}", A);
			return position += 2;
		}
	}
	private static int JumpIfTrue(int[] intcode, int position, int[] opcode=null)
	{
		//Console.WriteLine("Jump if True");
		// true and no opcode
		if (opcode==null)
		{
			if (intcode[intcode[position+1]]!=0)
			{
				return intcode[ intcode[position+2] ];
			}
			else
			{
				return position += 3;
			}
		}
		else
		{
			if (ParameterReader(intcode, position+1, opcode[opcode.Length-3])!=0)
			{
				return ParameterReader(intcode, position+2, opcode[opcode.Length-4]);
			}
			else
			{
				return position += 3;
			}
		}
	}
	private static int JumpIfFalse(int[] intcode, int position, int[] opcode=null)
	{
		//Console.WriteLine("Jump if False");
		// no opcode
		if (opcode==null)
		{
			if (intcode[intcode[position+1]]==0)
			{
				return intcode[ intcode[position+2] ];
			}
			else
			{
				return position += 3;
			}
		}
		// opcode given
		else
		{
			if (ParameterReader(intcode, position+1, opcode[opcode.Length-3])==0)
			{
				return ParameterReader(intcode, position+2, opcode[opcode.Length-4]);
			}
			else
			{
				return position += 3;
			}
		}
	}
	private static int LessThan(int[] intcode, int position, int[] opcode=null)
	{
		//Console.WriteLine("Less than");
		if (opcode==null)
		{
			int A = intcode[intcode[position+1]];
			int B = intcode[intcode[position+2]];
			int C = intcode[position+3];
			if (A<B)
			{
				intcode[C] = 1;
			}
			else
			{
				intcode[C] = 0;
			}
			return position += 4;
		}
		else
		{
			int A = ParameterReader(intcode, position+1, opcode[opcode.Length-3]);
			int B = ParameterReader(intcode, position+2, opcode[opcode.Length-4]);
			//int C = ParameterReader(intcode, position+3, opcode[opcode.Length-5]);
			int C = intcode[position+3];
			if (A<B)
			{
				intcode[C] = 1;
			}
			else
			{
				intcode[C] = 0;
			}
			return position += 4;
		}
	}
	private static int EqualsInt(int[] intcode, int position, int[] opcode=null)
	{
		//Console.WriteLine("Equals");
		if (opcode == null)
		{
			int A = intcode[intcode[position+1]];
			int B = intcode[intcode[position+2]];
			int C = intcode[position+3];
			if (A==B)
			{
				intcode[C] = 1;
			}
			else
			{
				intcode[C] = 0;
			}
			return position += 4;
		}
		else
		{
			int A = ParameterReader(intcode, position+1, opcode[opcode.Length-3]);
			int B = ParameterReader(intcode, position+2, opcode[opcode.Length-4]);
			//int C = ParameterReader(intcode, position+3, opcode[opcode.Length-5]);
			int C = intcode[position+3];
			if (A==B)
			{
				intcode[C] = 1;
			}
			else
			{
				intcode[C] = 0;
			}
			return position += 4;
		}
	}
	private static int[] FillOutOpcode(int rawOpcode)
	{
		int opcodeLength = AoC19.IntLength(rawOpcode);
		int[] opcode = AoC19.Int2IntArray(rawOpcode);
		//int opcodeOperation = opcode[opcodeLength-1];
		//Console.WriteLine("opcodeOperation = {0}", opcodeOperation);

		// check the neccesary length of the opcode (3 or 2) depending on operation
/*		if (opcodeOperation==1 || opcodeOperation==2 || opcodeOperation==7 || opcodeOperation==8)
		{
			// fill out complete opcode including leading zeroes
			int[] opcodeReturn = new int[5];
			for (int i=1; i<=opcodeLength; i++)
			{
				opcodeReturn[opcodeReturn.Length-i] = opcode[opcodeLength-i];
			}
			return opcodeReturn;
		}
		else if (opcodeOperation==4)
		{
			// fill out complete opcode including leading zeroes
			int[] opcodeReturn = new int[4];
			for (int i=1; i<=opcodeLength; i++)
			{
				opcodeReturn[opcodeReturn.Length-i] = opcode[opcodeLength-i];
			}
			return opcodeReturn;
		}
		return opcode;*/
		int[] opcodeReturn = new int[5];
		for (int i=1; i<=opcodeLength; i++)
		{
			opcodeReturn[opcodeReturn.Length-i] = opcode[opcodeLength-i];
		}
		return opcodeReturn;
	}
	private static int ParameterMode(int[] input, int position)
	{
		//Console.WriteLine("raw opcode = {0}", input[position]);

		// parse opcode to array
		int[] opcode = FillOutOpcode(input[position]);
		int opcodeLength = opcode.Length;
		// 
		if (opcode[opcodeLength-1] == 1)
		{
			return Addition(input, position, opcode);
		}
		else if (opcode[opcodeLength-1] == 2)
		{
			return Multiplication(input, position, opcode);
		}
		else if (opcode[opcodeLength-1] == 3)
		{
			return UserInput(input, position, opcode);
		}
		else if (opcode[opcodeLength-1] == 4)
		{
			return PrintOut(input, position, opcode);
		}
		else if (opcode[opcodeLength-1] == 5)
		{
			return JumpIfTrue(input, position, opcode);
		}
		else if (opcode[opcodeLength-1] == 6)
		{
			return JumpIfFalse(input, position, opcode);
		}
		else if (opcode[opcodeLength-1] == 7)
		{
			return LessThan(input, position, opcode);
		}
		else if (opcode[opcodeLength-1] == 8)
		{
			return EqualsInt(input, position, opcode);
		}
		else
		{
			Console.WriteLine("raw parameter mode : {0}", input[position]);
			throw new System.ArgumentException("\n Parameter mode not read as useable, {input[position]}");
		}
	}
	private static int ParameterReader(int[] input, int position, int Mode)
	{
		if (Mode == 1)
		{
			return input[position];
		}
		else
		{
			return input[ input[position] ];
		}
	}
} // AoC19
