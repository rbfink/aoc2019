using System;
using static System.Console;
using static System.Math;
using System.Collections.Generic;

public static partial class AoC19
{
	public static List<int[]> WireCrossings(List<string[]> input)
	{
		/*
		Input  : Wire directions with formatting (L82, R56, U788, D9213, ...)
		L: left, R: right, U: up, D: down
		Output : List<int[]> of (int x,int y) coordinates for wire crossing points
		*/
		
		/* Determine size of matrix */
		int RL = 0;
		int UD = 0;
		int R_max = 0;
		int L_max = 0;
		int U_max = 0;
		int D_max = 0;
		char direction = ' ';
		int distance = new int();
		/* loop over lines */
		for (int line=0; line<input.Count; line++)
		{
			/* loop over entrances */
			for (int step=0; step<input[line].Length; step++)
			{
				/* parse input string */
				direction = input[line][step][0]; 
				distance = Int32.Parse( input[line][step].Substring(1) );
				/* find matrix size relative to Origo */
				if (direction == 'R')
				{
					RL += distance;
					if (RL > R_max){R_max = RL;}
				}
				else if (direction == 'L')
				{
					RL -= distance;
					if (RL < L_max){L_max = RL;}
				}
				else if (direction == 'U')
				{
					UD += distance;
					if (UD > U_max){U_max = UD;}
				}
				else
				{
					UD -= distance;
					if (UD < D_max){D_max = UD;}
				}
			}// step
		}// lines
		WriteLine("matrix dim : {0}, {1}", U_max+Abs(D_max), R_max+Abs(L_max));
		/* generate matrix */
//		int[,] matrix = new int[U_max+Abs(D_max), R_max+Abs(L_max)];
		int[,] matrix = new int[30000, 30000]; // hardcoded for now
		int[] pos = new int[2];
		/* walk the paths */
		for (int line=0; line<input.Count; line++)
		{
			WriteLine("Line {0}", line+1);
			/* start in "Origo" */
//			pos[0] = U_max;
//			pos[1] = Abs(L_max);

			// hardcoded for now
			pos[0] = 15000;
			pos[1] = 15000;
			WriteLine("starting position [{0}, {1}]", pos[0], pos[1]);
			for (int step=0; step<input[line].Length; step++)
			{
				/* parse input string */
				direction = input[line][step][0]; 
				distance = Int32.Parse( input[line][step].Substring(1) );
				/* walk */
//				WriteLine(" step : {0}", step);
				if (direction == 'R')
				{
//					WriteLine("R steps to take {0}", distance);
					for (int i=pos[1]; i<(pos[1]+distance); i++)
					{matrix[pos[0], i] += line+1;}
					pos[1] += distance;
				}
				else if (direction == 'L')
				{
//					WriteLine("L steps to take {0}", distance);
					for (int i=pos[1]; i>(pos[1]-distance); i--)
					{matrix[pos[0], i] += line+1;}
					pos[1] -= distance;
				}
				else if (direction == 'U')
				{
//					WriteLine("U steps to take {0}", distance);
					for (int i=pos[0]; i>(pos[0]-distance); i--)
					{matrix[i, pos[1]] += line+1;}
					pos[0] -= distance;
				}
				else
				{
//					WriteLine("D steps to take {0}", distance);
					for (int i=pos[0]; i<(pos[0]+distance); i++)
					{matrix[i, pos[1]] += line+1;}
					pos[0] += distance;
				}
//				WriteLine("{0}, {1} \n", pos[0],pos[1]);
			}// step
		}// line

		/* Walk through one line and check for crossings ie. matrix[x,y] == 3 */
		List<int[]> crossingCoord = new List<int[]>();
		/* Origo for manhattan distance calculation*/
		// hardcoded for now...
		crossingCoord.Add(new int[] {15000, 15000});
		pos[0] = 15000;
		pos[1] = 15000;
		/* only need to follow one wire since there is only 2 wires */
		for (int step=0; step<input[1].Length; step++)
		{
			/* parse input string */
			direction = input[1][step][0]; 
			distance = Int32.Parse( input[1][step].Substring(1) );
			if (direction == 'R')
			{
				for (int i=pos[1]; i<(pos[1]+distance); i++)
				{
					if (matrix[pos[0], i] == 3)
					{
						crossingCoord.Add(new int[] {pos[0], i});
					}
				}
				pos[1] += distance;
			}
			else if (direction == 'L')
			{
				for (int i=pos[1]; i>(pos[1]-distance); i--)
				{
					if (matrix[pos[0], i] == 3)
					{
						crossingCoord.Add(new int[] {pos[0], i});
					}
				}
				pos[1] -= distance;
			}
			else if (direction == 'U')
			{
				for (int i=pos[0]; i>(pos[0]-distance); i--)
				{
					if (matrix[i, pos[1]] == 3)
					{
						crossingCoord.Add(new int[] {i, pos[1]});
					}
				}
				pos[0] -= distance;
			}
			else
			{
				for (int i=pos[0]; i<(pos[0]+distance); i++)
				{
					if (matrix[i, pos[1]] == 3)
					{
						crossingCoord.Add(new int[] {i, pos[1]});
					}
				}
				pos[0] += distance;
			}

		}
		/* write coordinates into array */

		return crossingCoord;
	}// WireCrossings
}// AoC19
