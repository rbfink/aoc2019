using System;
using System.Collections.Generic;
using static System.Console;

public static partial class AoC19
{
    public static int MinimumOrbitTransfers(string[] orbitArray, string[] transferPair)
    {
        if (transferPair.Length!=2)
        {
            throw new System.ArgumentException($"\n !!! string[] transferpair input not formatted correctly !!!");
        }
        // create Dictionary
        int orbitCount = orbitArray.Length;
        Dictionary<string,string> orbitDictionary = new Dictionary<string, string>(orbitCount);
        // fill out dictionary with direct orbits
        for (int i=0; i<orbitCount; i++)
        {
            int orbitIndex = orbitArray[i].IndexOf(")");
            orbitDictionary.Add(orbitArray[i].Substring(orbitIndex+1), orbitArray[i].Substring(0,orbitIndex));
        }
        // calculate minimum orbit transfers between transferPair of planets
        List<string> planet1orbits = FillOrbits(orbitDictionary,transferPair[0]);
        List<string> planet2orbits = FillOrbits(orbitDictionary,transferPair[1]);
        int minimumOrbitTransfers = 0;
        for (int i=0; i<planet1orbits.Count; i++)
        {
            // check if planet orbits intersect
            if (planet2orbits.Contains(planet1orbits[i]))
            {
                string orbitIntersect = planet1orbits[i];
                string orbitsAround = planet1orbits[1];
                int planet2Distance = 1;
                while (orbitsAround != orbitIntersect)
                {
                    planet2Distance += 1;
                    orbitsAround = planet2orbits[planet2Distance];
                }
                minimumOrbitTransfers = i+planet2Distance;
                break;
            }
        }
        return minimumOrbitTransfers;
    } // MinimumOrbitTransfers
    
    private static List<string> FillOrbits(Dictionary<string,string> orbitDictionary, string planet)
    {
        List<string> orbitList = new List<string>();
        //int orbits2COM = 0;
        string orbitsAround = orbitDictionary[planet];
        orbitList.Add(orbitsAround);
        while(orbitsAround != "COM") // Center Of Mass
        {
            orbitList.Add(orbitDictionary[orbitsAround]);
            orbitsAround = orbitDictionary[orbitsAround];
        }
        return orbitList;
    } // FillOrbits
} // AoC19