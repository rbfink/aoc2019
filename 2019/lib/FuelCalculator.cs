
public partial class AoC19
{
	public static int FuelCalculator(int mass)
	{
		int fuel = (int)System.Math.Floor(mass/3.0) - 2;
		return fuel;
	}
}
