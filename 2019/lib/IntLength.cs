using System;

public static partial class AoC19
{
    public static int IntLength(int input)
    {
        int i = Math.Abs(input);
        if (i==0)
        {
            return 1;
        }
        return (int)Math.Floor(Math.Log10(i)) + 1;
    }
}