using System;
using System.Collections.Generic;
using static System.Console;

public static partial class AoC19
{
    public static int OrbitCountChecksum(string[] orbitArray)
    {
        int orbitCount = orbitArray.Length;
        Dictionary<string,string> orbitDictionary = new Dictionary<string, string>(orbitCount);

        // fill out dictionary with direct orbits
        for (int i=0; i<orbitCount; i++)
        {
            int orbitIndex = orbitArray[i].IndexOf(")");
            orbitDictionary.Add(orbitArray[i].Substring(orbitIndex+1), orbitArray[i].Substring(0,orbitIndex));
        }
        // calculate direct and indirect orbits for checksum
        {
            int orbitCountChecksum = 0;
            string orbitsAround;
            foreach(KeyValuePair<string,string> orbit in orbitDictionary)
            {
                if (orbit.Value == "COM") // Center Of Mass
                {
                    orbitCountChecksum += 1;
                    continue;
                }
                orbitsAround = orbit.Value;
                while(orbitsAround != "COM") // Center Of Mass
                {
                    orbitCountChecksum += 1;
                    orbitsAround = orbitDictionary[orbitsAround];
                }
                orbitCountChecksum += 1;
            }

            return orbitCountChecksum;
        }
    } // OrbitCountChecksum
} // AoC19