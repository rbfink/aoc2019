using System;

public static class main
{
    public static int Main(string[] args)
    {
        // check if input is given
        if (args.Length == 0)
		{throw new System.ArgumentException($"\n No argument given. Please give file containning IntCode");}
        
        // read data
        int[] input = DataReader.ReadOneLine2Int(args[0],args[1]);

        for (int i=0; i<5; i++)
        {
            AoC19.IntCodeReader(input,false);
        }


        return 0;
    }
}