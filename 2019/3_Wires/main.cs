using System;
using System.Collections.Generic;
using static System.Console;

public static class main
{
	public static int Main(string[] args)
	{
		if (args.Length == 0)
		{throw new ArgumentException($"\n no argument given. Please give a data file");}

		WriteLine();
		WriteLine(" DAY 3 : Wires");
		WriteLine();
		WriteLine("--- part 1 ---");

		/* read data input */
		List<string[]> data = DataReader.ReadFile2ListString(args[0], args[1]);
		/* find wire crossing points */
		List<int[]> wireCrossings = AoC19.WireCrossings(data);
		/* calculate minimum manhattan distance */
		int manHattanDist = new int();
		int minManhattanDist = Int32.MaxValue;
		//WriteLine(wireCrossings.Count);
		for (int i=2; i<wireCrossings.Count; i++)
		{
			manHattanDist = AoC19.ManhattanDist(wireCrossings[0], wireCrossings[i]);
			//WriteLine("wireCrossings[{0}] = [{1},{2}] ",i,wireCrossings[i][0],wireCrossings[i][1]);
			if ( manHattanDist < minManhattanDist)
			{
				minManhattanDist = manHattanDist;
			}
		}

		WriteLine("Minimum Manhattan distance is {0}", minManhattanDist);

		WriteLine();
		WriteLine("--- part 2 --- ");
		WriteLine();

		return 0;
	}// Main
}// main
