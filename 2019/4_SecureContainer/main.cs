using System;
using static System.Console;

public static class main
{
    public static int Main(string[] args)
    {
        /* checking there is given an input */
        if (args.Length == 0)
        {throw new ArgumentException($"\n no argument given, please give an input data file");}
        
        /* Read password range, filename and seperator givin in args[] */
        int[] passwordRange = DataReader.ReadOneLine2Int(args[0], args[1]);

        int useablePasswords = 0;

        // part 1
        for (int i=passwordRange[0]; i<=passwordRange[1]; i++)
        {
            if (AoC19.PasswordCheck(i, false))
            {useablePasswords++;}
        }
        WriteLine();
        WriteLine(" DAY 4 : Secure Container");
        WriteLine();
        WriteLine("--- part 1 ---");
        WriteLine("password range = {0}-{1}", passwordRange[0],passwordRange[1]);
        WriteLine("useable passwords = {0}", useablePasswords);

        // part 2
        useablePasswords = 0;
        for (int i=passwordRange[0]; i<=passwordRange[1]; i++)
        {
            if (AoC19.PasswordCheck(i, true))
            {useablePasswords++;}
        }

        WriteLine();
        WriteLine("--- part 2 ---");
        WriteLine("password range = {0}-{1}", passwordRange[0],passwordRange[1]);
        WriteLine("useable passwords = {0}\n", useablePasswords);
        WriteLine();

        return 0;
    }
}