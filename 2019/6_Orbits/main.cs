using System;
using static System.Console;

public static class main
{
    public static int Main(string[] args)
    {
        if (args.Length == 0)
		{throw new System.ArgumentException($"\n No argument given. Please give file containning IntCode");}
        
        // read data
        string[] orbits = DataReader.ReadFile2StringArray(args[0]);

        // part 1 : calculate orbit check sum
        int orbitCheckSum = AoC19.OrbitCountChecksum(orbits);

        // part 2 : 
        string[] transferPair = new string[]{"YOU", "SAN"};
        int minimumOrbitTransfers = AoC19.MinimumOrbitTransfers(orbits, transferPair);

        WriteLine();
        WriteLine(" Day 6 : Orbits");
        WriteLine();
        WriteLine("--- part 1 ---");
        WriteLine("Orbit count checksum = {0}", orbitCheckSum);
        WriteLine();

        WriteLine();
        WriteLine("--- part 2 ---");
        WriteLine("Necessary orbit transfers between YOU and SAN : {0}", minimumOrbitTransfers);
        WriteLine();

        return 0;
    }
}