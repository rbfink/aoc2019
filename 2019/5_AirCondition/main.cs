using System;
using static System.Console;

public static class main
{
    public static int Main(string[] args)
    {
        if (args.Length == 0)
		{throw new System.ArgumentException($"\n no argument given. Please give file containning IntCode");}

        
        WriteLine();
        WriteLine(" DAY 5 : Aircondition");
        WriteLine();
        WriteLine("--- part 1 --- (provide 1 as input)");

        // read data
        int[] intCode = DataReader.ReadOneLine2Int(args[0], args[1]);

        // run IntCode program
        AoC19.IntCodeReader(intCode);

        WriteLine();
        WriteLine("--- part 2 --- (provide 5 as input)");

        // reset data
        int[] intCodeP2 = DataReader.ReadOneLine2Int(args[0], args[1]);

        // run IntCode program
        AoC19.IntCodeReader(intCodeP2);
        
        /*
        int[] testEqualsPos = new int[]{3,9,8,9,10,9,4,9,99,-1,8};
        int[] testLessPos = new int[]{3,9,7,9,10,9,4,9,99,-1,8};
        int[] testEqualsImm = new int[]{3,3,1108,-1,8,3,4,3,99};
        int[] testLessImm = new int[]{3,3,1107,-1,8,3,4,3,99};

        int[] test1 = new int[]{3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9};
        int[] test2 = new int[]{3,3,1105,-1,9,1101,0,0,12,4,12,99,1};

        AoC19.IntCodeReader(testEqualsPos,false);
        AoC19.IntCodeReader(testLessPos,false);
        AoC19.IntCodeReader(testEqualsImm,false);
        AoC19.IntCodeReader(testLessImm,false);
        //AoC19.IntCodeReader(test1);
        //AoC19.IntCodeReader(test2);
        */

        return 0;
    }
}