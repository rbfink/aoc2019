using static System.Console;
using static AoC19;
using System.Collections.Generic;

public static class main
{
	public static int Main(string[] args)
	{
		if (args[0] != "data.txt"){
			throw new System.ArgumentException("\n No proper data filename given as input !");
		}
		WriteLine();
		WriteLine(" Day 1 : Fuel for Takeoff");
		WriteLine();
		/* ----------------------------------------------------------- Part 1 */
		WriteLine("--- Part 1 ---");
		/* naive fuel caluclation */
		int totFuel = 0;
		List<int[]> massList = DataReader.ReadFile2ListInt(args[0]);
		for (int i=0; i<massList[0].Length; i++){
			totFuel += FuelCalculator(massList[0][i]);
		}
		WriteLine($"Fuel needed for takeoff without taking fuel mass into account = {totFuel}");

		/* ----------------------------------------------------------- Part 1 */
		WriteLine();
		WriteLine("--- Part 2 ---");
		/* taking the fuels mass into account */
		int tottotFuel = 0;
		for (int i=0; i<massList[0].Length; i++){
			int moduleFuel = FuelCalculator(massList[0][i]);
			int recFuel = FuelCalculator(moduleFuel);
			while (recFuel > 0)
			{				
				moduleFuel = moduleFuel + recFuel;
				recFuel = FuelCalculator(recFuel);
			}
			tottotFuel += moduleFuel;
		}
		WriteLine($"Fuel needed for takeoff when taking fuel mass into account = {tottotFuel}\n");
		
		return 0;
	}
}
