# README #

### Advent of Code 2019 ###

This repo contains my solutions to the 2019 Advent of Code (AoC) challenges written in C#.

### Run check of all solutions ###

After cloning and entering the repo, you can simply type "make" in the terminal and follow any instructions to see all current solved problems print out the solutions for each daily challenge

```shell
$ make
```